#LmwProject Base

##Start with Command Line
1. mkdir build && cd build
2. cmake .. -DCMAKE_BUILD_TYPE=Release/Build
3. cmake --build . --config Release/Build
4. ctest

or use build.py 

##Start with Visual Studio
Simply open Folder with Visual Studio 

#Third Party Code
- We use Google Test library from https://github.com/google/googletest.git v1.10.0 Own License form 