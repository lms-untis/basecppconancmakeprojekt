import os
from sys import platform


def getBuildFolder() -> str:
    if "win" in platform:
        folderName = 'buildWindows'
        if not os.path.isdir(folderName):
            os.mkdir(folderName)
        return folderName
    elif "linux" in platform:
        folderName = "buildLinux"
        if not os.path.isdir(folderName):
            os.mkdir(folderName)
        return folderName


def generate(config):
    buildFolder = getBuildFolder()
    os.system("cd " + buildFolder +
              " && cmake .. -DCMAKE_BUILD_TYPE=" + config)


def build(config):
    buildFolder = getBuildFolder()
    os.system("cd " + buildFolder + " && cmake --build . --config " + config)


def runTests():
    buildFolder = getBuildFolder()
    os.system("cd " + buildFolder +
              " && ctest")


if __name__ == '__main__':
    build("Release")
