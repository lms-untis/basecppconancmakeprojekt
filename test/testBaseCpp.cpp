#include "gtest/gtest.h"

#include "BaseCpp.h"

class TBaseCpp : public ::testing::Test
{
public:
    const std::string baseFolder = "bumbastik";
};

TEST_F(TBaseCpp, giveMeFive)
{
    EXPECT_EQ(giveMeFive(), 5);
}