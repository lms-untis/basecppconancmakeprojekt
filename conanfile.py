from conans import ConanFile, CMake, tools


class CsvHighschoolFinnland(ConanFile):
    name = "LmwProjekt"
    version = "1.0"
    author = "Lukas Wolf"
    url = ""
    description = ""
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake", "cmake_find_package", "cmake_paths"
    requires = ("gtest/1.10.0")
    exports_sources = "*"

    def build(self):
        cmake = CMake(self)
        print(cmake.build_config)
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='bin', src='lib')
        self.copy("*.dll", dst="Release", src="bin")
        self.copy("*.dll", dst="Debug", src="bin")

    def package(self):
        self.copy("*.h", dst="include")
        self.copy("*.hpp", dst="include")
        self.copy("*LmwProjekt.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy(".so", dst="lib", keep_path=False)
        self.copy(".dylib", dst="lib", keep_path=False)
        self.copy(".a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["LmwProjekt"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libdirs = ["lib"]
