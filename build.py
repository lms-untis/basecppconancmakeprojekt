import argparse
import python.build as b


class BuildParser:
    def __init__(self):
        self._args = self._get_args()

    def _get_args(self):
        parser = argparse.ArgumentParser(
            description="Run Untis Optimization Performance Test",
            formatter_class=argparse.RawTextHelpFormatter,
        )
        parser.add_argument(
            "-c",
            "--config",
            help="Configuration of Untis Build (Default Release)",
            default="Release",
            choices=["Release", "Debug"],
        )
        parser.add_argument("-b", "--build", action="store_true",
                            default=False, help="Only Build without Generating. (Default Off)")
        parser.add_argument("-t", "--test", action="store_true",
                            default=False, help="Run Tests. (Default Off)")
        args = parser.parse_args()
        return args

    def get_config(self):
        return self._args.config

    def is_test_on(self):
        return self._args.test

    def is_only_build(self):
        return self._args.build


if __name__ == '__main__':
    parser = BuildParser()
    if not parser.is_only_build():
        b.generate(parser.get_config())
    b.build(parser.get_config())
    if parser.is_test_on():
        b.runTests()
